%global _enable_debug_packages %{nil}
%global debug_package          %{nil}
Summary: Adventure and investigation game based on Agatha Christie novel
Name: abcmurders
Version: 2.2.0.4
Release: 1
URL: http://www.agathachristie.microids.com/
Source0: https://cdn-hw.gog.com/secure/agatha_christie_abc_murders/linux/gog_agatha_christie_the_abc_murders_2.2.0.4.sh
Source1: %{name}.desktop
License: GOG
BuildArch: i686
BuildRequires: bsdtar
BuildRequires: desktop-file-utils
Requires: libGL.so.1
Requires: libpulse-simple.so.0
Requires: libudev.so.1
Requires: libX11.so.6
Requires: libXcursor.so.1
Requires: libXrandr.so.2
Requires: mesa-dri-drivers%{_isa}

%global __provides_exclude_from ^%{_libdir}/%{name}/The ABC Murders_Data/(Plugins|Mono)/x86/.*\\.so$
%global __requires_exclude ^libsteam_api.so$

%description
The ABC Murders is an adventure and investigation game adapted from the classic
Agatha Christie novel. The player embodies the famous Hercule Poirot in a 3rd
person perspective adventure game packed with mysteries. Once again, the private
detective will find himself up against a mysterious opponent who goes by the
name of "ABC". Your intelligence will never have been so challenged!

You will have to explore many crime scenes in various cities set in beautiful
surroundings across the United Kingdom. Leave no stone unturned when it comes to
cross examinations and deadly puzzles!

Observe, question and explore everything possible in order to make the smartest
deductions and understand the murderer’s plans!

%prep
%setup -qcT
bsdtar -x -f%{S:0}
find "data/noarch/game/The ABC Murders_Data" -type f -print0 | xargs -0 chmod -x
chmod +x data/noarch/game/The\ ABC\ Murders_Data/{Mono,Plugins}/x86/*.so

%install
install -dm755 %{buildroot}{%{_bindir},%{_libdir}/%{name}}
cp -pr data/noarch/game/The\ ABC\ Murders_Data %{buildroot}%{_libdir}/%{name}
install -pm755 data/noarch/game/The\ ABC\ Murders.x86 %{buildroot}%{_libdir}/%{name}

install -Dpm644 data/noarch/support/icon.png %{buildroot}%{_datadir}/icons/hicolor/256x256/apps/%{name}.png
desktop-file-install --dir %{buildroot}%{_datadir}/applications %{S:1}

%files
%license "data/noarch/docs/End User License Agreement.txt"
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/256x256/apps/%{name}.png
%{_libdir}/%{name}

%changelog
* Sat Mar 14 2020 Dominik Mierzejewski <rpm@greysector.net> 2.2.0.4-1
- update to 2.2.0.4
- use bsdtar to unpack upstream archive
- use included icon
- add dependencies not linked directly

* Sun Nov 20 2016 Dominik Mierzejewski <rpm@greysector.net> 2.2.0.3-1
- update to 2.2.0.3
- add missing libudev dependency

* Mon Jun 13 2016 Dominik Mierzejewski <rpm@greysector.net> 2.1.0.2-1
- initial build
